
package eu.ictontic.cssp

import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.{linalg => sla}
import org.apache.spark.mllib.linalg.{Matrix, Matrices, Vectors, DenseMatrix, DenseVector}
import org.apache.spark.mllib.linalg.distributed.{RowMatrix, MatrixEntry, CoordinateMatrix}
import breeze.{linalg => bla}
import breeze.numerics._
import breeze.linalg.qrp.QRP
import org.apache.spark.storage.StorageLevel
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.util.matching.Regex

import java.text.SimpleDateFormat;
import java.util.Date;

/**
  * @author Bruno Ordozgoiti
  */
object App {

  val mandatoryArgs = List("k", "file")
  val optionalArgs = List(
    ("mp", "2"),
    ("scaling", "PR"),
    ("sampling", "OWN"), 
    ("samples", "-1"), 
    ("sv", "-1"),
    ("samples", "40"),
    ("svd", "FALSE"), 
    ("std", "TRUE")
  ).toMap
 
  def main(args : Array[String]) {

    //Parse the arguments
    val argPairs = optionalArgs ++ args.map(x => {
      val p = (x.tail tail) split "="      
      (p(0), p(1))
    }).toMap

    //Check that arguments are correct. Otherwise exit
    if ( mandatoryArgs.filter(argPairs.map(_._1).toList.contains(_)).length < mandatoryArgs.length || 
      !(argPairs.map(_._1).toList.filter(x => !(mandatoryArgs.contains(x) || optionalArgs.map(_._1).toList.contains(x))).isEmpty) ){
      Helper.printUsage
      return
    }

    val logger = LoggerFactory.getLogger("csspLogger")
    logger.trace("########################################")
    logger.trace("ARGS")
    argPairs.map(x => logger.trace(x._1 + " = " + x._2))
    logger.trace("########################################")

    val file = argPairs("file")
    val K = argPairs("k").toInt
    val minPartitions = argPairs("mp").toInt
    val SCALING = argPairs("scaling")
    val SAMPLING = argPairs("sampling")
    val SV_ENTRY = argPairs("sv").toInt
    val SAMPLES_ENTRY = argPairs("samples").toInt
    val DO_SVD = argPairs("svd")
    val DO_STD = argPairs("std")

    val conf = new SparkConf().setAppName("Twostage_scala")

    val sc = new SparkContext(conf)

    logger.trace("**************************************************");
    logger.trace("PICS: Parallelized Independent Column Selection");
    logger.trace("||||| => |||");
    logger.trace(":D");
    logger.trace("Starting job. Configuration:");
    for(t <- sc.getConf.getAll){
      logger.trace(t._1 + ": " + t._2);
    }
    logger.trace("**************************************************");

    //*********************************
    // Data input and preprocessing
    //*********************************
    val readStart = System.currentTimeMillis
    val distFile = sc.textFile(file, minPartitions)
    val readEnd = System.currentTimeMillis
    logger.trace("Input read time: " + (readEnd-readStart)/1000.0)

    val prerows = distFile.filter(x =>("[^0-9|.|\\s]".r findFirstIn x) == None).map(s => {
      val row = s.split(" ")
      Vectors.dense(row.map(_.toDouble))
    }).persist(StorageLevel.MEMORY_ONLY)
  
    val M = prerows.count
    val N = prerows.first.size
    val FACTOR = math.log(K);
    val C = math.min(math.max(2, K*FACTOR), N).toInt    
    val SAMPLES = SAMPLES_ENTRY match{
      case -1 => math.min((math.pow(2, math.sqrt(0.3*N-K))*2), 40).toInt
      case _ => SAMPLES_ENTRY
    }
    val SV = SV_ENTRY match{
      case -1 => K
      case _ => SV_ENTRY
    }

    logger.trace("M: " + M)
    logger.trace("N: " + N)
    logger.trace("K: " + K)
    logger.trace("C: " + C)
    logger.trace("Random samples: " + SAMPLES)

    val stdStart = System.currentTimeMillis
    
    val rows = DO_STD match {
      case "TRUE" => {
        val colAvgs = prerows.map(x => new bla.DenseVector(x.toArray)).reduce((a,b) => a + b).map(_ / M)
        val bcColAvgs = sc.broadcast(colAvgs)
        val colSds = prerows.map(x => {
          val avgs = bcColAvgs.value
          val v = new bla.DenseVector(x.toArray) - avgs
          (v :* v)
        }).reduce((a,b) => a + b).map(x => if (x == 0.0) 1 else math.sqrt(x/(M-1)))
        val bcColSds = sc.broadcast(colSds)
        
        prerows.map(x => {
          val sds = bcColSds.value
          val avgs = bcColAvgs.value
          val v = (new bla.DenseVector(x.toArray) - avgs) :/ sds
          //new org.apache.spark.mllib.linalg.DenseVector(v.toArray)
          Vectors.dense(v.toArray)
        }).persist(StorageLevel.MEMORY_ONLY)
      }
      case "FALSE" => prerows
    }

    val stdEnd = System.currentTimeMillis
    logger.trace("Standardization time: " + (stdEnd-stdStart)/1000.0)

    if (DO_STD == "FALSE"){
      prerows.unpersist(false)
    }

    //*********************************
    //*** The algorithm starts here ***
    val start = System.currentTimeMillis

    val sVectors = SAMPLING match{
      case "TRAD" => K
      case "OWN" => math.min(N, SV)
    }

    //Compute the SVD
    val usv = new RowMatrix(rows).computeSVD(sVectors, true, 1e-24)
    val singularVectors = new sla.DenseMatrix(usv.V.numRows, usv.V.numCols, usv.V.toArray)
    val singularValues = new sla.DenseVector(usv.s.toArray)

    //Test numerical rank with the specified tolerance
    val compRank = singularValues.toArray.filter(_ > 1e-2).length
    logger.trace("Numerical rank at least " + compRank)

    //Sampling probabilities
    val amV = new bla.DenseMatrix(N toInt, sVectors toInt, singularVectors.toArray)
    val mV = SAMPLING match{
      case "TRAD" => amV(::, Range(0, K))
      case "OWN" => amV * bla.diag(new bla.DenseVector(singularValues.toArray))
    }
    val probabilities = Range(0, N).map(i => math.pow(bla.norm(mV(i, ::).t),2))//.map(x => x/K.toDouble)
    val normProbs = probabilities.map(x => x/probabilities.sum)
    logger.trace("Probabilities")
    logger.trace("" + normProbs)
    val colSamples = this.randomlySample(Range(0,N).toList, normProbs.toList, C, SAMPLES)

    val vT = singularVectors.transpose
    //For each candidate subset, build the corresponding column sample
    //of Sigma V^T
    val chosenColumnLists = colSamples.map(s => {
      val sampledArray = s.map(x => Range(0,K).map(y => vT.apply(y,x) * (SCALING match {
        case "BOUT" => 1/(math.sqrt(math.min(1,C*probabilities(x))))
        case "PR" => math.pow(singularValues(y), 1)
        case "NONE" => 1
      }))).flatten.toArray
      val dm = new bla.DenseMatrix(K, s.length, sampledArray)
      //Run the RRQR and keep the first k columns
      val QRP(_QQ, _RR, _P, _pvt) = bla.qrp(dm)
      _pvt.take(K).map(x => s(x))
    })

    val choices = chosenColumnLists.filter(_.length >= K)
    val allChosenColumns = choices.map(_ toSet).reduce((x,y) => x union y).toList.sorted

    //Norm minimization phase: Calculate Qt A
    val bcAllChosen = sc.broadcast(allChosenColumns)
    val qtA = rows.map(row => {
      val allChosen = bcAllChosen.value
      val qRow = row.toArray.zipWithIndex.filter(allChosen contains _._2).map(_._1)
      val v1 = new bla.DenseMatrix(qRow.size, 1, qRow.toArray)
      val v2 = new bla.DenseMatrix(1, row.size, row.toArray)
      v1 * v2
    }).reduce((x,y) => x + y)
    logger.trace("Size of Q: " + qtA.rows + ", " + qtA.cols)
    
    //Extract the multiple instances of Ct C from Qt A by eliminating columns and
    //rows
    //approximators' structure: ((List of chosen columns, CpA), index)
    val approximators = choices.map(x =>{
      //Take the indices of the columns of Q that are in this C
      val colsInC = allChosenColumns.zipWithIndex.filter(x contains _._1).map(_._2)
      val ctc = qtA(colsInC.sorted, x.toList.sorted).toDenseMatrix
      val r = bla.rank(ctc)
      (x, ctc, r)
    }).filter(x => x._3 == K).map(x => {
      val ctc = x._2
      val colsInC = allChosenColumns.zipWithIndex.filter(x._1 contains _._1).map(_._2)
      val ctci = bla.inv(ctc)
      val ctA = qtA(colsInC, ::).toDenseMatrix
      List((x._1, ctci * ctA))
    }).reduce((x,y) => x ::: y).zipWithIndex

    //Finally, calculate the residual norm for all choices of C
    val bcApprox = sc.broadcast(approximators)
    val residualNorms = rows.map(row => {
      val aRow = new bla.DenseVector(row.toArray)
      val approxs = bcApprox.value
      //diffs' structure: (column choice index, norm of difference
      //between row of A and row of approximated A). List of tuples. 
      val diffs = approxs.map(x => {
        val cRow = aRow.toArray.zipWithIndex.filter(y => x._1._1 contains y._2).map(y => y._1)
        val cRowM = new bla.DenseMatrix(1, cRow.size, cRow.toArray)
        val cpa = x._1._2
        val approx = cRowM * cpa
        val d = (new bla.DenseVector(approx.toArray) - aRow)
        (x._2, (d :* d).sum)
      })
      diffs
    }).reduce((a,b) => ((a ::: b).groupBy(_._1).map(kv => (kv._1, kv._2.map(_._2).sum))).toList )

    val end = System.currentTimeMillis
    logger.trace("Algorithm time: " + (end-start)/1000.0)

    val winner = residualNorms.minBy(_._2)._1
    val minimumNorm = math sqrt (residualNorms.minBy(_._2)._2)
    logger.trace("Chosen subset: " + choices(winner).toList)
    logger.trace("Residual frobenius norm: " + minimumNorm)

    //Rebuild the matrix with SVD to obtain the lower bound to the
    //residual norm
    if(DO_SVD == "TRUE"){
      val svdStart = System.currentTimeMillis
      val dataPairs = rows.zipWithIndex.map(x => (x._2, x._1.toDense))
      val rmData = new RowMatrix(rows)
      val svdF = rmData.computeSVD(K, true, 1e-9)
      val sigmaVT = new DenseMatrix(svdF.s.size, N.toInt, svdF.V.transpose.toArray.zipWithIndex.map(x => x._1 * svdF.s((x._2 % svdF.s.size).toInt)))
      val aPrime = svdF.U.multiply(sigmaVT)
      val apPairs = aPrime.rows.zipWithIndex.map(x => (x._2, x._1.toDense))//rows.map(x => (x.index, x.vector.toDense))
      val apDataPairs = apPairs.join(dataPairs).values
      val residualNormSVD = math.sqrt(
        apDataPairs.map(rows => {
          val cRow = new bla.DenseVector(rows._1.toArray)
          val aRow = new bla.DenseVector(rows._2.toArray)
          val d = (cRow - aRow)
            (d :* d).sum
        }).reduce((x,y) => x+y)
      )
      val svdEnd = System.currentTimeMillis
      logger.trace("SVD residual time: " + (svdEnd-svdStart)/1000.0)
      logger.trace("Residual frobenius norm with SVD: " + residualNormSVD)
    }

   sc.stop

  }

  def randomlySample(l : List[Int], d : List[Double], c : Int, samples : Int) : List[Array[Int]] = {
    var choices = new Array[Array[Int]](samples)
    for (i <- 0 to samples-1){
      var s = Set[Int]()
      var choice = new Array[Int](c)
      for (j <- 0 to c-1) {
        val distMap = (l zip d).filter(x => !(s contains x._1)).sortBy(_._2).reverse
        val top = distMap.map(_._2).reduce((a,b) => a+b)
        val vals = distMap.map(x => x._2)
        val indexedVals = vals.zipWithIndex
        val acum = indexedVals.map(x => (distMap(x._2)._1, (vals take x._2).sum))
        
        val r = scala.util.Random.nextDouble * top
        
        choice(j) = acum.filter(x => x._2 <= r).last._1
        s = s union Set(choice(j))
      }
      choices(i) = choice.distinct
    }
    return choices.toList
  }
}

object Helper {
  val logger = LoggerFactory.getLogger("csspLogger")
  def printUsage(){
    val s =     "\r\n\r\n" +
    "----------\r\n" +
    "PICS: Parallelized Independent Column Selection." +
    "\r\n\r\n" +
    "Usage: spark-submit <jar-file>.jar --file=<input file> --k=<number of chosen columns> [--mp=<minpartitions> --scaling=BOUT|PR --sampling=TRAD|OWN --samples=<integer> --svd=TRUE|FALSE --std=TRUE|FALSE]" +
    "\r\n\r\n" +
    "--mp=<Integer>\n" + 
    "\t The value of minPartitions for the input file.\n" + 
    "--scaling=BOUT|PR|NONE\n" + 
    "\t The approach for scaling the input of the RRQR (default PR).\n" + 
    "--sampling=TRAD|OWN\n" + 
    "\t The approach for sampling candidate subsets (default OWN).\n" + 
    "--samples=<Integer>\n" + 
    "\t The number of candidates to sample.\n" + 
    "--svd=TRUE|FALSE\n" +
    "\t If TRUE, compute the residual for the best rank-k approximation at the end of the algorithm (default FALSE).\n" + 
    "--std=TRUE|FALSE]\n" +
    "\t If TRUE, standardize the data to zero mean and unit variance (default TRUE).\n"

    println(s)
    logger.trace(s)
  }

  def toBreezeDenseMatrix(v : org.apache.spark.mllib.linalg.DenseMatrix) : bla.DenseMatrix[Double] = {
    return new bla.DenseMatrix[Double](v.numRows, v.numCols, v.toArray)
  }

}
